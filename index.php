<?php
	
	// registrasi class
	include_once('config.php');
	include_once('database.php');
	include_once('todo.php');
	include_once('user.php');

	// registrasi object
	$db = new database(DB_HOST,DB_NAME,DB_USER,DB_PASS);
	$todo = new todo;
	$user = new user;

	// set view data
	$data = array(
		'list_user' => $user->list(), 
	);

	// view
	include_once ('home.php');

?>